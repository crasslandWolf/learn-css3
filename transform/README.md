# 学习transfrom(变形，改变)笔记

## 浏览器支持(来自w3c)

| IE                            | Firefox           | Chrome             | Safari             | Opera             |
| :---------------------------- | :---------------- | :----------------  | :----------------- | :---------------- |
| IE10+                         | 支持              | 支持               | 支持               | 支持              |
| IE9支持代替的-ms-transform    | -moz-transform    | -webkit-transform  | -webkit-transform  | -o-transform      |

## 定义和用法

  transform属性向元素应用2D或3D转换，主要包括rotate(旋转)、skew(扭曲)、scale(缩放)、translate(移动)、matrix(矩阵变形)
  
  
  语法:
  ```
    transform: none | <transform-function>;
    
    transform: rotate skew scale translate matrix;
  ```
  