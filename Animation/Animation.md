# 学习Animation(动画属性)笔记

## animation

### 作用: 简写属性，将动画与目标元素绑定

浏览器支持: 

w3c 解释:IE10、Firefox和opear支持animation属性，safari和chrome支持代替的-webkit-animation属性，IE9以及以下的都不支持。不过目前好像chrome是支持不带前缀的。

### 定义和用法

animation 是一个简写的属性，用于设置6个动画属性

| 项目      |    价格 | 数量  |
| :-------- | --------:| :--: |
| Computer  | 1600 元 |  5   |
| Phone     |   12 元 |  12  |
| Pipe      |    1 元 | 234  |

